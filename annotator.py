# coding: utf-8
import urllib
import json
import os
import argparse
import Tkinter as tk
import cv2
from PIL import Image
from PIL import ImageTk
from change_perspective import change_perspective

RATIO = 0.5
RADIUS = 30

parser = argparse.ArgumentParser()
parser.add_argument('-f', '--file-info', type=str, help='json dump with scene_id, photo_id, and file_name')
parser.add_argument('-out', '--output-name', type=str, default='final_info_perspective', help='name of the output file with perspective transform info')
parser.add_argument('-d', '--download-imgs', type=str, default='yes', help='Download images')
args = parser.parse_args()


def parse_imgs(info, img_dst='./data'):

    data = json.load(open(info))
    imgs_info = {}
    dwnldr = urllib.URLopener()

    dwnldd = []
    for dirpath, subdirs, filess in os.walk(img_dst):
            for name in filess:
                if name.endswith(".jpg") or name.endswith(".png") or name.endswith('jpeg'):
                    dwnldd.append(name)
    dwnldd = set(dwnldd)

    for idx, img_dict in enumerate(data):
        img_name = str(img_dict['scene_id']) + '_' + str(img_dict['photo_id']) + '_' + img_dict['file_name'] + '.jpg'
        
        if img_name in dwnldd:
            print 'Already downloaded: ', img_name
        else:
            print '{:3} / {} Downloading {}'.format(idx+1, len(data), img_dict['url'])
            dwnldr.retrieve(img_dict['url'], os.path.join(img_dst, img_name))
        del img_dict['url']
        img_dict['file_name'] = img_name
        imgs_info[img_name] = img_dict
        

    with open(info + '_tmp', 'wb') as f:
        json.dump(imgs_info, f)

    return imgs_info


# Ugly workaround ¯\_(ツ)_/¯
if os.path.isfile(args.file_info + '_tmp') and args.download_imgs == 'no':
    imgs_info = json.load(open(args.file_info + '_tmp'))
elif args.download_imgs == 'yes':
    imgs_info = parse_imgs(args.file_info)
else:
    print 'Wrong argument for --download: ', args.download_imgs, '.  Must be either yes or no'
    raise NameError('')


class RectTracker:
    def __init__(self, canvas):
        self.canvas = canvas
        self.item = None

    def draw(self, start, end, **opts):
        """Draw the rectangle"""
        return self.canvas.create_rectangle(*(list(start) + list(end)), **opts)

    def autodraw(self, **opts):
        """Setup automatic drawing; supports command option"""
        self.start = None
        self.a1 = self.canvas.bind("<Button-1>", self.__update, '+')
        self.a2 = self.canvas.bind("<B1-Motion>", self.__update, '+')
        self.a3 = self.canvas.bind("<ButtonRelease-1>", self.__stop, '+')
        self._command = opts.pop('command', lambda *args: None)
        self.rectopts = opts

    def undo(self):
        self.canvas.unbind("<Button-1>", self.a1)
        self.canvas.unbind("<B1-Motion>", self.a2)
        self.canvas.unbind("<ButtonRelease-1>", self.a3)

    def __update(self, event):
        if not self.start:
            self.start = [event.x, event.y]
            return

        if self.item is not None:
            self.canvas.delete(self.item)
        self.item = self.draw(self.start, (event.x, event.y), **self.rectopts)
        self._command(self.start, (event.x, event.y))

    def __stop(self, event):
        global x, y
        self.start = None
        self.canvas.delete(self.item)
        self.item = None


class DIP(tk.Frame):
    def __init__(self, parent):
        self.points = []
        self.points_global = []
        self.points_temp = []
        self.point_c = 0
        self.parent = parent
        self.images = []
        self.graphs = []
        self.maps = []
        self.image_count = 0
        self.parent.title("Box Annotator 1.0.0")

        ready = []
        path = "./warped"
        for dirpath, subdirs, filess in os.walk(path):
            for name in filess:
                if name.endswith(".jpg") or name.endswith(".png") or name.endswith('jpeg'):
                    ready.append(name)
        ready = set(ready)

        path = "./data"
        for dirpath, subdirs, filess in os.walk(path):
            for name in filess:
                if (name.endswith(".jpg") or name.endswith(".png") or name.endswith('jpeg')) and name not in ready:
                    self.images.append(os.path.join(dirpath, name))

        global imgs_info
        self.imgs_info = imgs_info

        self.img = cv2.imread(self.images[self.image_count])
        self.h_core, self.w_core, self.c_core = self.img.shape

        # Canvas
        self.WIDTH = int(self.w_core * RATIO)
        self.HEIGHT = int(self.h_core * RATIO)
        self.canvas = tk.Canvas(self.parent, width=self.WIDTH, height=self.HEIGHT, bd=5)
        self.canvas.grid(row=0, column=0, rowspan=10)

        self.setImage()
        self.run()

    def setImage(self):
        self.points_global = []
        if self.image_count != 0:
            for idx, graph in enumerate(self.graphs):
                for key in graph[1]:
                    p = graph[1][key]
                    # print "1", p
                    p = self.small2big(p, self.maps[idx])
                    # print "2", p
                    p = self.big2orig(p)
                    # print "3", p
                    self.graphs[idx][1][key] = (p)
            if len(self.graphs) != 0:
                # create_annot(self.graphs, self.images[self.image_count-1])
                change_perspective(self.graphs, self.images[self.image_count - 1], imgs_info=self.imgs_info)

            else:
                img = cv2.imread(self.images[self.image_count - 1])
                cv2.imwrite(self.images[self.image_count - 1].replace('/data/', '/warped/'), img)
            self.graphs = []

        if self.image_count == len(self.images):
            ds = []
            for info in os.listdir('./infos'):
                ds.append(json.load(open('./infos/' + info)))
            json.dump(ds, open(args.output_name, 'wb'))
            print "That's all"


            exit()
        self.img = cv2.imread(self.images[self.image_count])
        self.h_core, self.w_core, self.c_core = self.img.shape
        self.img = cv2.cvtColor(self.img, cv2.COLOR_BGR2RGB)
        self.img = cv2.resize(self.img, (self.WIDTH, self.HEIGHT))
        self.img_clean = self.img.copy()
        self.h, self.w, self.c = self.img.shape
        np_img = Image.fromarray(self.img)
        self.image_count += 1
        self.canvas.image = ImageTk.PhotoImage(np_img)
        self.canvas.create_image(0, 0, image=self.canvas.image, anchor='nw')
        self.maps = []

    def finishObject(self):

        if len(self.points) % 2 == 1:
            del self.points[-1]

        self.h, self.w, self.c = self.img.shape
        for i in range(len(self.points) / 2):
            x1 = int(self.points[2 * i][0] / float(self.WIDTH) * (self.x2 - self.x1)) + self.x1
            x2 = int(self.points[2 * i + 1][0] / float(self.WIDTH) * (self.x2 - self.x1)) + self.x1
            y1 = int(self.points[2 * i][1] / float(self.HEIGHT) * (self.y2 - self.y1)) + self.y1
            y2 = int(self.points[2 * i + 1][1] / float(self.HEIGHT) * (self.y2 - self.y1)) + self.y1
            self.points_global.append((x1, y1))
            self.points_global.append((x2, y2))
            self.img = cv2.line(self.img, (x1, y1), (x2, y2), (0, 0, 255), 1)
            self.img = cv2.circle(self.img, (x1, y1), 5, (0, 255, 0), -1)
            self.img = cv2.circle(self.img, (x2, y2), 5, (0, 255, 0), -1)

        self.canvas.unbind('<Button-1>', self.c1)
        self.canvas.unbind('<Control-z>', self.c2)
        self.canvas.unbind("<1>", self.c3)

        self.graphs.append([self.points_graph, self.points_dict])
        self.points = []
        self.points_temp = []
        self.point_c = 0

        np_img = Image.fromarray(self.img)
        self.canvas.image = ImageTk.PhotoImage(np_img)
        self.canvas.create_image(0, 0, image=self.canvas.image, anchor='nw')
        # self.img[self.y1:self.y2, self.x1:self.x2, :] = cv2.resize(self.part_img, (self.w_p, self.h_p))
        self.renew_image()
        self.run()

    def renew_image(self):

        img = cv2.resize(self.img, (self.WIDTH, self.HEIGHT))
        np_img = Image.fromarray(img)

        self.canvas.image = ImageTk.PhotoImage(np_img)
        self.canvas.create_image(0, 0, image=self.canvas.image, anchor='nw')

    def draw_bg(self):
        for i in range(len(self.points_global) / 2):
            x, y = self.points_global[2 * i]
            dot_x1 = x - self.x1
            dot_y1 = y - self.y1
            dot_x1 = int(dot_x1 / float(self.x2 - self.x1) * self.WIDTH)
            dot_y1 = int(dot_y1 / float(self.y2 - self.y1) * self.HEIGHT)

            x, y = self.points_global[2 * i + 1]
            dot_x2 = x - self.x1
            dot_y2 = y - self.y1
            dot_x2 = int(dot_x2 / float(self.x2 - self.x1) * self.WIDTH)
            dot_y2 = int(dot_y2 / float(self.y2 - self.y1) * self.HEIGHT)

            retval, p1, p2 = cv2.clipLine((0, 0, self.WIDTH, self.HEIGHT), (dot_x1, dot_y1), (dot_x2, dot_y2))

            self.part_img = cv2.line(self.part_img, p1, p2, (255, 0, 0), 1)

            if dot_y1 > 0 and dot_y1 < self.HEIGHT and dot_x1 > 0 and dot_x1 < self.WIDTH:
                self.part_img = cv2.circle(self.part_img, (dot_x1, dot_y1), 5, (255, 0, 0), -1)
            if dot_y2 > 0 and dot_y2 < self.HEIGHT and dot_x2 > 0 and dot_x2 < self.WIDTH:
                self.part_img = cv2.circle(self.part_img, (dot_x2, dot_y2), 5, (255, 0, 0), -1)

    def drawPoint(self):

        self.part_img = cv2.resize(self.part_img, (self.WIDTH, self.HEIGHT))
        self.draw_bg()
        np_img = Image.fromarray(self.part_img)

        self.canvas.image = ImageTk.PhotoImage(np_img)
        self.canvas.create_image(0, 0, image=self.canvas.image, anchor='nw')

    def run(self):

        # Canvas lines
        def cool_design(event):
            global x, y
            kill_xy()
            dashes = [3, 2]
            x = self.canvas.create_line(event.x, 0, event.x, self.HEIGHT, dash=dashes, tags='no')
            y = self.canvas.create_line(0, event.y, self.WIDTH, event.y, dash=dashes, tags='no')

        def kill_xy(event=None):
            self.canvas.delete('no')

        # Canvas Rectangle
        self.rect = RectTracker(self.canvas)

        def onDrag(start, end):
            global x, y

        self.rect.autodraw(fill="", width=2, command=onDrag)

        # Point track
        self.b1 = self.canvas.bind("<Button-1>", self.start, '+')
        self.b2 = self.canvas.bind("<ButtonRelease-1>", self.stop, '+')
        self.b3 = self.canvas.bind("<ButtonRelease-1>", self.enlarge, '+')
        self.b4 = self.canvas.bind('<Motion>', cool_design, '+')

        # Buttons
        self.next_img = tk.Button(self.parent, text="Next Image", command=self.setImage)
        self.next_img.grid(row=1, column=1)
        self.finish = tk.Button(self.parent, state=tk.DISABLED, text="Finish", command=self.finishObject)
        self.finish.grid(row=8, column=1)

    def start(self, event):
        global x, y
        self.x_start = event.x
        self.y_start = event.y

    def stop(self, event):
        global x, y
        self.x_end = event.x
        self.y_end = event.y

    def enlarge(self, event):

        self.points_graph = {}
        self.points_dict = {}

        self.x_start = max(self.x_start, 0)
        self.x_start = min(self.x_start, self.w)

        self.x_end = max(self.x_end, 0)
        self.x_end = min(self.x_end, self.w)

        self.y_start = max(self.y_start, 0)
        self.y_start = min(self.y_start, self.w)

        self.y_end = max(self.y_end, 0)
        self.y_end = min(self.y_end, self.w)

        self.x1 = min(self.x_end, self.x_start)
        self.x2 = max(self.x_end, self.x_start)
        self.y1 = min(self.y_end, self.y_start)
        self.y2 = max(self.y_end, self.y_start)

        self.maps.append((self.x1, self.y1, self.x2, self.y2))

        # print self.x1, self.y1, self.x2, self.y2
        self.rect.undo()
        self.undo()

        if self.x2 - self.x1 < 10 or self.y2 - self.y1 < 10:
            self.run()
        else:
            self.part_img = self.img_clean[self.y1:self.y2, self.x1:self.x2, :].copy()
            self.h_p, self.w_p, self.c_p = self.part_img.shape
            self.drawPoint()
            self.dots()

    def undo(self):

        self.canvas.unbind("<Button-1>", self.b1)
        self.canvas.unbind("<ButtonRelease-1>", self.b2)
        self.canvas.unbind("<ButtonRelease-1>", self.b3)
        self.canvas.unbind('<Motion>', self.b4)

    def small2big(self, p, points):
        x1, y1, x2, y2 = points
        x, y = p
        xx1 = int((x * (x2 - x1)) / float(self.WIDTH)) + x1
        yy1 = int((y * (y2 - y1)) / float(self.HEIGHT)) + y1
        # print "small2big:", p, (xx1, yy1)
        return xx1, yy1

    def big2small(self, p):
        x, y = p
        x1 = int((x - self.x1) / float((self.x2 - self.x1)) * self.WIDTH)
        y1 = int((y - self.y1) / float(self.y2 - self.y1) * self.HEIGHT)
        # print "big2small:", p, (x1, y1)
        return x1, y1

    def big2orig(self, p):
        x, y = p
        x1 = int(x / float(RATIO))
        y1 = int(y / float(RATIO))
        # print "big2orig:", p, (x1, y1)
        return x1, y1

    def orig2big(self, p):
        x, y = p
        x1 = int(x * RATIO)
        y1 = int(y * RATIO)
        # print "orig2big:", p, (x1, y1)
        return x1, y1

    def dots(self):
        self.finish.configure(state=tk.NORMAL)
        self.next_img.configure(state=tk.DISABLED)

        def draw(event):
            global x, y
            self.points_temp.append((event.x, event.y))
            self.part_img = cv2.circle(self.part_img, (event.x, event.y), 5, (0, 255, 0), -1)
            self.part_img = cv2.circle(self.part_img, (event.x, event.y), RADIUS, (255, 0, 0))
            self.drawPoint()
            self.point_c += 1
            if self.point_c == 1:
                self.finish.configure(state=tk.DISABLED)
            if self.point_c == 2:
                self.finish.configure(state=tk.NORMAL)
                self.canvas.unbind('<Button-1>', self.c1)
                self.canvas.unbind('<Control-z>', self.c2)
                self.canvas.unbind("<1>", self.c3)
                p1, p2 = self.add_graph()
                self.points.append(p1)
                self.points.append(p2)
                # print "debug:",self.points
                reset(None, False)
                self.part_img = cv2.line(self.part_img, p1, p2, (0, 0, 255), 1)
                self.drawPoint()
                self.points_temp = []
                self.point_c = 0
                self.dots()

        def reset(event, delete=True):
            # print "\ndelete1:", self.points
            self.part_img = self.img_clean[self.y1:self.y2, self.x1:self.x2, :].copy()
            self.drawPoint()
            # print "delete:", self.points
            if self.point_c == 0:
                self.point_c = 2
                if len(self.points) != 0:
                    if delete == True:
                        self.delete_line(self.points[-1], self.points[-2])
            # print "delete2:", self.points
            if len(self.points) != 0:
                for i in range(self.point_c):
                    if delete == True and self.point_c != 1:
                        if len(self.points) != 0:
                            del self.points[-1]

                for i in range(len(self.points) / 2):
                    self.part_img = cv2.circle(self.part_img, self.points[2 * i], RADIUS, (255, 0, 0))
                    self.part_img = cv2.circle(self.part_img, self.points[2 * i + 1], RADIUS, (255, 0, 0))
                    self.part_img = cv2.circle(self.part_img, self.points[2 * i], 5, (0, 255, 0), -1)
                    self.part_img = cv2.circle(self.part_img, self.points[2 * i + 1], 5, (0, 255, 0), -1)
                    self.part_img = cv2.line(self.part_img, self.points[2 * i], self.points[2 * i + 1], (0, 0, 255), 1)
            if delete == True:
                self.points_temp = []
                self.point_c = 0
            self.drawPoint()

        self.c1 = self.canvas.bind("<Button-1>", draw, '+')
        self.c2 = self.canvas.bind("<Control-z>", reset, '+')
        self.c3 = self.canvas.bind("<1>", lambda event: self.canvas.focus_set(), '+')

    def find_point(self, coor, ds=RADIUS):

        # Search for points within the radius of ds, if there is, return that point
        x, y = coor
        closest = {}
        for key in self.points_dict:
            x0 = self.points_dict[key][0]
            y0 = self.points_dict[key][1]
            dist = ((x - x0) * (x - x0) + (y - y0) * (y - y0))
            if dist < (ds * ds):
                closest[dist] = key

        if len(closest) > 0:
            # Returns closest one
            return closest[min(closest.keys())]
        else:
            # New point is added
            new_point = max(self.points_dict.keys()) + 1 if len(self.points_dict) > 0 else 0
            self.points_dict[new_point] = (x, y)
            return new_point

    def add_graph(self):

        coor1 = self.points_temp[0]
        coor2 = self.points_temp[1]

        # coor1 = (int(coor1[0]/float(self.WIDTH)*self.w_core), int(coor1[1]/float(self.HEIGHT)*self.h_core))
        # coor2 = (int(coor2[0]/float(self.WIDTH)*self.w_core), int(coor2[1]/float(self.HEIGHT)*self.h_core))
        # Check if point already exists
        point1 = self.find_point(coor1)
        point2 = self.find_point(coor2)

        # Add point1->point2
        if point1 not in self.points_graph:
            self.points_graph[point1] = {point2}
        else:
            self.points_graph[point1].add(point2)

        # Add point2->point1
        if point2 not in self.points_graph:
            self.points_graph[point2] = {point1}
        else:
            self.points_graph[point2].add(point1)

        # print "\nADD:"
        # print self.points_dict
        return [self.points_dict[point1], self.points_dict[point2]]

    def delete_line(self, coor1, coor2):

        # Find points to delete
        point1 = self.find_point(coor1)
        point2 = self.find_point(coor2)

        # Remove from graph
        self.points_graph[point1].remove(point2)
        self.points_graph[point2].remove(point1)

        # Remove points that do not connect to other points
        for key in self.points_graph.keys():
            if len(self.points_graph[key]) == 0:
                self.points_graph.pop(key)
                self.points_dict.pop(key)

                # print "\nDELETE:"
                # print self.points_dict


def main():
    root = tk.Tk()
    my_gui = DIP(root)
    root.geometry("1080x720")
    root.mainloop()


if __name__ == '__main__':
    main()
