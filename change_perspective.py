import numpy as np
import cv2
import json

def order_points(pts):
    # initialzie a list of coordinates that will be ordered
    # such that the first entry in the list is the top-left,
    # the second entry is the top-right, the third is the
    # bottom-right, and the fourth is the bottom-left
    rect = np.zeros((4, 2), dtype="float32")

    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    # return the ordered coordinates
    return rect


def check_graphs(graphs):

    neighs_dict = graphs[0][0]
    points_visited = {}

    # Check if graph is empty
    if not len(neighs_dict) > 0:
        print 'Input neighbors graph is empty'
        return False

    # Walk the graph, check if every point has only 2 neighbors, and keep track of visited points
    walk_point = neighs_dict.keys()[0]
    while walk_point not in points_visited:

        if not len(neighs_dict[walk_point]) == 2:
            print 'It should have exactly 2 neighbors'
            return False

        points_visited[walk_point] = 1
        if list(neighs_dict[walk_point])[0] in points_visited:
            walk_point = list(neighs_dict[walk_point])[1]
        else:
            walk_point = list(neighs_dict[walk_point])[0]

    # Check if there are only 4 visited points, and number of visited points equals to all points in the graph
    if not (len(points_visited) == 4 and len(points_visited) == len(neighs_dict)):
        print 'There is mismatch in number of total points'
        return False

    return True


def transform_points(points, m):
    '''
    Given transformational matrix, it maps points into new space
    :param points: List of 2D points. [[x1, y1], [x2, y2], ...[x_n, y_n]]
    :param m: Transformation matrix
    :return: Transformed points. [[x1, y1], [x2, y2], ...[x_n, y_n]]
    '''

    # Check if points are not empty
    assert len(points) > 0, 'No points were found'

    # Make every point 3 dimensional (x, y, 1)
    pre_points = []
    for point in points:
        if len(point) == 2:
            pre_points.append(tuple(point) + (1,))
        elif len(point) == 3:
            pre_points.append(point)
        else:
            raise ValueError('Point must be either 2D or 3D')
    pre_points = np.array(pre_points, dtype=np.int32)

    # Transform points using matrix
    trns_points = m.dot(pre_points.T).T

    # Normalize points
    nrm = []
    for pt in trns_points:
        pt = (pt / pt[2]).astype(np.int32)
        nrm.append((pt[0], pt[1]))

    return nrm


def change_perspective(graphs, img_name, imgs_info=None):
    print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
    print img_name

    if check_graphs(graphs):
        img = cv2.imread(img_name)

        # Obtain the coordinates from dict
        coors = []
        for point in graphs[0][1].keys():
            coors.append(graphs[0][1][point])

        rect = order_points(np.array(coors))

        (tl, tr, br, bl) = rect

        # Update images info
        if imgs_info is not None:
            imgs_info[img_name.split('/')[-1]]['x1'] = str(int(tl[0]))
            imgs_info[img_name.split('/')[-1]]['x2'] = str(int(tr[0]))
            imgs_info[img_name.split('/')[-1]]['x3'] = str(int(br[0]))
            imgs_info[img_name.split('/')[-1]]['x4'] = str(int(bl[0]))
            imgs_info[img_name.split('/')[-1]]['y1'] = str(int(tl[1]))
            imgs_info[img_name.split('/')[-1]]['y2'] = str(int(tr[1]))
            imgs_info[img_name.split('/')[-1]]['y3'] = str(int(br[1]))
            imgs_info[img_name.split('/')[-1]]['y4'] = str(int(bl[1]))
            json.dump(imgs_info[img_name.split('/')[-1]], open(img_name.replace('/data/', '/infos/')[:-4], 'wb'))
        elif img_name.split('/')[-1] not in imgs_info:
            img_info = {}
            img_info['x1'] = str(int(tl[0]))
            img_info['x2'] = str(int(tr[0]))
            img_info['x3'] = str(int(br[0]))
            img_info['x4'] = str(int(bl[0]))
            img_info['y1'] = str(int(tl[1]))
            img_info['y2'] = str(int(tr[1]))
            img_info['y3'] = str(int(br[1]))
            img_info['y4'] = str(int(bl[1]))
            json.dump(img_info, open(img_name.replace('/data/', '/infos/')[:-4], 'wb'))


        # compute the width of the new image, which will be the
        # maximum distance between bottom-right and bottom-left
        # x-coordiates or the top-right and top-left x-coordinates
        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))

        # compute the height of the new image, which will be the
        # maximum distance between the top-right and bottom-right
        # y-coordinates or the top-left and bottom-left y-coordinates
        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))

        # now that we have the dimensions of the new image, construct
        # the set of destination points to obtain a "birds eye view",
        # (i.e. top-down view) of the image, again specifying points
        # in the top-left, top-right, bottom-right, and bottom-left
        # order
        dst = np.array([
            [0, 0],
            [maxWidth - 1, 0],
            [maxWidth - 1, maxHeight - 1],
            [0, maxHeight - 1]], dtype="float32")

        # compute the perspective transform matrix and then apply it
        M = cv2.getPerspectiveTransform(rect, dst)

        # Compute new coordinates of the original image edges
        tl_img = (0, 0, 1)
        bl_img = (0, img.shape[0], 1)
        tr_img = (img.shape[1], 0, 1)
        br_img = (img.shape[1], img.shape[0], 1)
        pts = np.array([tl_img, tr_img, br_img, bl_img], np.int32)
        new_pts = M.dot(pts.T).T

        check_points = [tl_img, tr_img, br_img, bl_img, [21, 52, 1]]
        check_points = transform_points(check_points, M)

        # Normalize obtained coordinates
        nrm = []
        for pt in new_pts:
            pt = (pt / pt[2]).astype(np.int32)
            nrm.append((pt[0], pt[1]))

        # Create new dst for image, make it (0,0) centered. The initial points are the edges of the image
        dst = np.array(nrm).astype(np.float32)
        dst -= np.amin(dst, axis=0)
        rect = np.array([tl_img[:2], tr_img[:2], br_img[:2], bl_img[:2]], dtype="float32")

        # Shape of final image
        shape = np.amax(dst, axis=0) - np.amin(dst, axis=0)

        # Recompute new transform matrix
        M = cv2.getPerspectiveTransform(rect, dst)

        warped = cv2.warpPerspective(img, M, tuple(shape), borderMode=cv2.BORDER_REPLICATE)

        new_name = img_name.replace('/data/', '/warped/')
        cv2.imwrite(new_name, warped)


    else:
        print 'Warning! The ', img_name, ' was not transformed'

    print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'